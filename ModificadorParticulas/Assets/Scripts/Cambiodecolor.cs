using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cambiodecolor : MonoBehaviour
{
    public ParticleSystem ps;

    public void cambiorojo()
    {
        var color = ps.main;
        color.startColor = Color.red;
    }

        public void cambioazul()
    {
        var color = ps.main;
        color.startColor = Color.blue;
    }

        public void cambioverde()
    {
        var color = ps.main;
        color.startColor = Color.green;
    }

        public void cambioamarillo()
    {
        var color = ps.main;
        color.startColor = Color.yellow;
    }

        public void apagar()
    {
        var emission = ps.emission;
        emission.enabled = false;
    }

            public void encender()
    {
        var emission = ps.emission;
        emission.enabled = true;
    }

}
